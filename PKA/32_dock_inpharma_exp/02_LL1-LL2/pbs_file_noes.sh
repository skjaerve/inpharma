#!/bin/bash
#PBS -l select=1:ncpus=8
#PBS -q clng_new
#PBS -r n

export LD_LIBRARY_PATH=/g/software/linux/pack/ifort-xe2011/lib/intel64:/g/software/linux/pack/ifort-xe2011/mkl/lib/intel64:/struct/carlomag/common/software/gcc/lib64:/struct/carlomag/common/software/lib:/usr/lib64:/struct/carlomag/common/software/gcc/lib/:/struct/carlomag/common/software/gcc/lib64/:/struct/carlomag/common/software/gcc/libexec/:/usr/lib:$LD_LIBRARY_PATH


tmp_dir=`mktemp -d -p /tmp`

echo `hostname`
echo ${tmp_dir}

python-2.7 /struct/carlomag/angelini/run/inpharma/support/file_noes.py ${config_file} ${tmp_dir}/

cp -r ${tmp_dir} ${output_dir}

#if [/usr/bin/diff -q ${tmp_dir} ${output_file}]; then
#        rm -f ${tmp_file}

