#!/bin/bash
base=`pwd`

dir[2]="02_LL1-LL2"
dir[3]="03_LL1-M77"
dir[4]="04_LL1-A14"
dir[5]="05_LL1-A62"
dir[6]="06_LL2-M77"
dir[7]="07_LL2-A14"
dir[8]="08_LL2-A62"
dir[9]="09_M77-A14"
dir[10]="10_M77-A62"
dir[11]="11_A14-A62"


for i in 3 6 
do
    cd $base
    cd ${dir[$i]}
    config_file=${base}/${dir[$i]}/config_file
    output_dir=${base}/${dir[$i]}/01_allRow
    qsub -v config_file=${config_file},output_dir=${output_dir} pbs_file_noes.sh

  cd $base
  cd ${dir[$i]}
done






