from multiInpharma import InpharmaRun, InpharmaComplex


import os

basepath = os.getcwd()

coms = [ InpharmaComplex("3dne.LL1.pdb", "LL1.asgn",  "LL1.hlist", "LL1", "LL1"),
         InpharmaComplex("3dnd.LL2.pdb", "LL2.asgn",  "LL2.hlist", "LL2", "LL2"), 
         InpharmaComplex("1q8w.M77.pdb", "M77.asgn",  "M77.hlist", "M77", "M77"), 
         #InpharmaComplex("2vny.M02.pdb", "M02.asgn",  "M02.hlist", "M02", "M02"),
         InpharmaComplex("A14P.A14.pdb", "A14.asgn",  "A14.hlist", "A14", "A14"),
         InpharmaComplex("A62P.A62.pdb", "A62.asgn",  "A62.hlist", "A62", "A62") ]
         

coms[1].kL = '30000.0'
#coms[2].kL = '30000.0'

receptorHlist = "receptor.hlist"

## prefix to the individual runs and directories to be made
variants = ['11']
##variants = ['02', '03', '04', '05', '06', '07', '08', '09' ]

## suffix to the namlist files listed in 01_prepare/namlists
namlist_suffix = ['01']
##namlist_suffix = ['01', '02', '03', '04', '05', '06', '07', '08' ]

k=1
for i in range(0,len(coms)-1):
    com1 = coms[i]
    
    for j in range(i+1,len(coms)):
        com2 = coms[j]
        k+=1
        combinationname = '%s_%s-%s' % (str(k).zfill(2), com1.name, com2.name)

        print 'Running combination: %s %s' % (com1.name, com2.name)

        os.chdir(basepath)
        run = InpharmaRun(com1, com2, receptorHlist, combinationname,
 						  variants, namlist_suffix)
            
        run.run()
