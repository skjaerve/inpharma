

source("/struct/carlomag/skjaerve/r-scripts/inpharma.R")

### Plotting individual NOEs
files.ref <- c("arr_noes_1_300_ref.dat", "arr_noes_1_500_ref.dat", "arr_noes_1_700_ref.dat")
files.calc <- c("arr_noes_1_300.dat", "arr_noes_1_500.dat", "arr_noes_1_700.dat")

pdf('noes_R.pdf')
plot.noes(files.ref, files.calc, plot.labels=TRUE, pattern=c("md\\.", "md\\.1"))
dev.off()



### Plotting Multiples data
source("/struct/carlomag/skjaerve/r-scripts/inpharma.R")

file1 <- "noes_strong_combined.dat"
file2 <- "noes_strong_avg.dat"

pdf('multiplexed_hits.pdf')
plot.multiplex(file1)
plot.multiplex(file2)
dev.off()
