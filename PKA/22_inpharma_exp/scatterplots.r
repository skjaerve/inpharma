
library(plotrix)
source("/struct/carlomag/skjaerve/r-scripts/inpharma.R")


result.suffix <- "03_shortRow/noes.dat"

inph.dirs <- c("02_LL1-LL2", "03_LL1-M77", "06_LL2-M77")

all.data <- NULL
for ( i in 1:length(inph.dirs) ) {

  datafile <- paste(inph.dirs[i], result.suffix, sep="/")
  data <- read.table(datafile, header=T)
  all.data[[i]] <- data
}


pdf("scatterplots_shortRow_wTwist.pdf", w=12, h=8)
par(mfcol=c(2,3), mar=c(3,2,3,2))

for ( i in 1:length(inph.dirs) ) {
  data <- all.data[[i]]
  mtext <- inph.dirs[i]
  
  plot.filter.hits(data, num.best=500,
                   ylim=c(0., 1), lnp.cutoff=5,
                   slope.cutoff=c(0., 1.5), q.cutoff=5,
                   mtext=mtext, plot = "R")

  plot.filter.hits(data, num.best=500,
                   ylim=c(0, 4), lnp.cutoff=5,
                   slope.cutoff=c(0., 1.5), q.cutoff=5,
                   mtext=mtext, plot = "LNP_M")


}

dev.off()





