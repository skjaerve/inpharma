
#library(bio3d)
source("/struct/carlomag/skjaerve/r-scripts/inpharma.R")
#source("/struct/carlomag/skjaerve/r-scripts/dock_funs.R")



result.suffix <- "/03_shortRow/noes.dat"

inph.dirs <- c("02_LL1-LL2", "03_LL1-M77",
               "06_LL2-M77") 

all.data <- NULL
for ( i in 1:length(inph.dirs) ) {

  datafile <- paste(inph.dirs[i], result.suffix, sep="")
  data <- read.table(datafile, header=T)
  all.data[[i]] <- data
}

r.best <- 100
lnp.best <- 100
lnp.cutoff <- 5
slope.cutoff <- c(0,2) 
q.cutoff <- 5 


# LL1
inph.list <- c(1,2)
result.inds <- c("File_1", "File_1")
hmm1 <- multiplex.intersect(all.data, inph.list, result.inds, result.suffix, lnp.cutoff=lnp.cutoff,
                               r.best=r.best, lnp.best=lnp.best, q.cutoff=q.cutoff, slope.cutoff=slope.cutoff)

## LL2
inph.list <- c(1,3)
result.inds <- c("File_2", "File_1")
hmm2 <- multiplex.intersect(all.data, inph.list, result.inds, result.suffix, lnp.cutoff=lnp.cutoff,
                            r.best=r.best, lnp.best=lnp.best, q.cutoff=q.cutoff, slope.cutoff=slope.cutoff)

# M77
inph.list <- c(2,3)
result.inds <- c("File_2", "File_2")
hmm3 <- multiplex.intersect(all.data, inph.list, result.inds, result.suffix, lnp.cutoff=lnp.cutoff,
                            r.best=r.best, lnp.best=lnp.best, q.cutoff=q.cutoff, slope.cutoff=slope.cutoff)

names <- c("LL1", "LL2", "M77")
pdf("multiplex_new2.pdf", w=12, h=6 )
plot.multiplex.intersect( list(hmm1, hmm2, hmm3), names )
dev.off()
