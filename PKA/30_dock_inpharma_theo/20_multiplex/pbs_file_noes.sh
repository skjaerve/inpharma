#!/bin/bash
#PBS -l select=1:ncpus=8:mem=8gb
#PBS -q clng_new
#PBS -r n

export LD_LIBRARY_PATH=/g/software/linux/pack/ifort-xe2011/lib/intel64:/g/software/linux/pack/ifort-xe2011/mkl/lib/intel64:/struct/carlomag/common/software/gcc/lib64:/struct/carlomag/common/software/lib:/usr/lib64:/struct/carlomag/common/software/gcc/lib/:/struct/carlomag/common/software/gcc/lib64/:/struct/carlomag/common/software/gcc/libexec/:/usr/lib:$LD_LIBRARY_PATH

config_file=/struct/carlomag/common/INPHARMA/30_dock_inpharma_theo/20_multiplex/config_file
output_dir=/struct/carlomag/common/INPHARMA/30_dock_inpharma_theo/20_multiplex/

tmp_dir=`mktemp -d -p /tmp`

echo `hostname`
echo ${tmp_dir}

python-2.7 /struct/carlomag/angelini/run/inpharma/support/multiplexing.py ${config_file} ${tmp_dir}/

cp -r ${tmp_dir} ${output_dir}

