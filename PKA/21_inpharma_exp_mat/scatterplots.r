
library(plotrix)
source("/struct/carlomag/skjaerve/r-scripts/inpharma.R")


result.suffix <- "/02_allRow/noes.dat"

inph.dirs <- c("02_LL1-LL2", "03_LL1-M77", "06_LL2-M77")
               

all.data <- NULL
for ( i in 1:length(inph.dirs) ) {

  datafile <- paste(inph.dirs[i], result.suffix, sep="")
  data <- read.table(datafile, header=T)
  all.data[[i]] <- data
}


pdf("scatterplots.pdf", w=12, h=4)
par(mfrow=c(1,3), mar=c(3,2,3,2))

for ( i in 1:length(inph.dirs) ) {
  data <- all.data[[i]]
  mtext <- inph.dirs[i]
  
  plot.filter.hits(data, num.best=100,
                   ylim=c(0.8, 1), lnp.cutoff=5,
                   slope.cutoff=c(0,2), q.cutoff=2,
                   mtext=mtext)
}

dev.off()





