#!/bin/bash

inpharma-alice2-hlist pdb D off ../02_structures/LL1.pdb > LL1.hlist
inpharma-alice2-hlist pdb D off ../02_structures/LL2.pdb > LL2.hlist
inpharma-alice2-hlist pdb D off ../02_structures/M77.pdb > M77.hlist
inpharma-alice2-hlist pdb D off ../02_structures/M02.pdb > M02.hlist
inpharma-alice2-hlist pdb D off ../02_structures/A14.pdb > A14.hlist
inpharma-alice2-hlist pdb D off ../02_structures/A62.pdb > A62.hlist

inpharma-alice2-hlist pdb D off ../02_structures/receptor.pdb > receptor.hlist


