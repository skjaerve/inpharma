#!/bin/bash

ln -s ../01_prepare_structures/01_3DNE_LL1/12_minimize_ref/min.amb.pdb 3dne.LL1.pdb
ln -s ../01_prepare_structures/02_3DND_LL2/12_minimize_ref/min.amb.pdb 3dnd.LL2.pdb
ln -s ../01_prepare_structures/03_1Q8W_M77/12_minimize_ref/min.amb.pdb 1q8w.M77.pdb
ln -s ../01_prepare_structures/04_2VNY_M02/12_minimize_ref/min.amb.pdb 2vny.M02.pdb
ln -s ../01_prepare_structures/05_A14/12_minimize_ref/min.amb.pdb A14P.A14.pdb
ln -s ../01_prepare_structures/06_A62/12_minimize_ref/min.amb.pdb A62P.A62.pdb

ln -s ../01_prepare_structures/01_3DNE_LL1/12_minimize_ref/LL1.amb.pdb LL1.pdb
ln -s ../01_prepare_structures/02_3DND_LL2/12_minimize_ref/LL2.amb.pdb LL2.pdb
ln -s ../01_prepare_structures/03_1Q8W_M77/12_minimize_ref/M77.amb.pdb M77.pdb
ln -s ../01_prepare_structures/04_2VNY_M02/12_minimize_ref/M02.amb.pdb M02.pdb
ln -s ../01_prepare_structures/05_A14/12_minimize_ref/A14.amb.pdb A14.pdb
ln -s ../01_prepare_structures/06_A62/12_minimize_ref/A62.amb.pdb A62.pdb


ln -s ../01_prepare_structures/01_3DNE_LL1/11_build_ref/receptor.amber.pdb receptor.pdb

