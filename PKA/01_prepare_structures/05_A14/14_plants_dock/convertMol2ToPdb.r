source("/struct/carlomag/skjaerve/r-scripts/dock_funs.R")
library(bio3d)

args=(commandArgs(TRUE))

pdb.target <- read.pdb("../receptor.min.amb.pdb")
inds.fit <- atom.select(pdb.target, "///71:73,105:106,121:130,169:174,184:185///N,CA,C/")
inds.rms <- atom.select(pdb.target, "///49:59,71:73,105:106,121:130,169:174,184:185///N,CA,C/")

recname <- args[1] 
ligandname <- args[2]
modes <- args[3]

ligand <- "../ligand.amber.pdb"
dockedprot <- "docked_proteins.mol2"
dockedlig <- "docked_ligands.mol2"

ligand.reference = read.pdb(ligand, het2atom = TRUE)
binding.modes.rec <- read.mol2(dockedprot)
binding.modes.lig <- read.mol2(dockedlig)

for ( k in 1:modes ) {
  print(k) 
  newxyz.rec = binding.modes.rec$xyz[k,]
  pdb.target$xyz = newxyz.rec
  
  newxyz.lig = binding.modes.lig$xyz[k,]
  ligand.reference$xyz = newxyz.lig

  complex = concat.pdb(pdb.target, ligand.reference)
  
  filename = paste(recname, ligandname, k, 'pdb', sep=".")
  filename = paste('pdbs_raw', filename, sep="/")
  write.pdb(complex$newpdb, file=filename)
}


