#!/bin/bash

# copy a reference pdb file to this dir 
# this file is used for structural alignment
# named: reference.amber.pdb

# Additionally required files:
# (reference target structure): reference.amber.pdb
# (sequence of xray_structure): target_sequence.ali 
# (amber build file): build
# (modeller files): 01_align.py
# (modeller files): 02_build_model.py
# (pymol alignment script): get_ligand.pml

# model can be babel, ideal, model. 
# the two latter use structure from ligand expo.
model="babel"
pdb="3DND"
ligand="LL2"

# check for required files
filenames[1]="reference.amber.pdb"
filenames[2]="target_sequence.ali"
filenames[3]="build"
filenames[4]="01_align.py"
filenames[5]="02_build_model.py"
filenames[6]="get_ligand.pml"

for i in 1 2 3 4 5 6 
do
	fn=${filenames[$i]}
    if [ ! -f $fn ] 
	then
		echo "Could not find file ${fn}" 
		exit 1
	fi
done


# fetch the ligands with Hydrogens from PDB
wget -Oxray.pdb http://www.pdb.org/pdb/files/${pdb}.pdb

### Build a full length humanPKA model ###
grep "^ATOM" xray.pdb > xray_ATOMs.pdb

python 01_align.py
python 02_build_model.py

mv target.B99990001.pdb xray_model.pdb

### Align the protein to the reference structure
### Fetch the ligand
sed 's/LIGANDID/'${ligand}'/g' get_ligand.pml > tmp_script.pml
pymol -cq tmp_script.pml

if [ $model == "babel" ]
then
    babel -h -ipdb ligand.pdb -opdb ligand_model.pdb
else
    wget -Oligand_model.pdb http://ligand-expo.rcsb.org/reports/${ligand:0:1}/${ligand}/${ligand}_${model}.pdb
fi

# Generate parameters for the ligand
$AMBERHOME/bin/antechamber -i ligand_model.pdb -fi pdb -o prepi -fo prepi -c bcc -s 2 -nc 0 
parmchk -i prepi -f prepi -o frcmod

tleap -f build

# Convert to PDB-files
ambpdb -p complex.prmtop < complex.inpcrd > complex.amber.pdb
ambpdb -p receptor.prmtop < receptor.inpcrd > receptor.amber.pdb

# Convert to SYBYL mol2-files
top2mol2 -p receptor.prmtop -c receptor.inpcrd -o receptor.sybyl.mol2
top2mol2 -p ligand.prmtop -c ligand.inpcrd -o ligand.sybyl.mol2

# Convert to PLANTS mol2-files
SPORES_64bit --mode settypes receptor.sybyl.mol2 receptor.plants.mol2
SPORES_64bit --mode settypes ligand.sybyl.mol2 ligand.plants.mol2 

# Fetch the ligand, and save to a seperate PDB file
grep ${ligand} complex.amber.pdb > ligand.amber.pdb

