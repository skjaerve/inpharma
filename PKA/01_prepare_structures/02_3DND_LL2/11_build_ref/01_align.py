from modeller import *

env = environ()
aln = alignment(env)
mdl = model(env, file='xray_ATOMs.pdb', model_segment=('FIRST:A','LAST:A'))
aln.append_model(mdl, align_codes='XRAY', atom_files='xray_ATOMs.pdb')
aln.append(file='target_sequence.ali', align_codes='target')
aln.align2d()
aln.write(file='alignment.ali', alignment_format='PIR')
aln.write(file='alignment.pap', alignment_format='PAP')

