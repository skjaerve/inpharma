#!/bin/bash

# Configure
pdb="3dnd"
ligand="LL2"

# Get ligand and receptor structures
ln -s ../11_build_ref/ligand.plants.mol2
ln -s ../12_minimize_ref/receptor.plants.mol2

ln -s ../12_minimize_ref/receptor.min.amb.pdb
ln -s ../11_build_ref/ligand.amber.pdb

LIGFILE="ligand.plants.mol2"
#RECFILE="receptor.plants.mol2"

# fetching files for MD simulation
cp ../11_build_ref/frcmod 00_input_files/ 
cp ../11_build_ref/prepi 00_input_files/

# Configure plants runs 
OUTDIR[1]="01_results_05A_rigid"
OUTDIR[2]="02_results_1A_rigid"
OUTDIR[3]="03_results_2A_rigid"
OUTDIR[4]="04_results_05A_flexSC"
OUTDIR[5]="05_results_1A_flexSC"
OUTDIR[6]="06_results_2A_flexSC"
OUTDIR[7]="07_results_05A_flexSC_flexLig"
OUTDIR[8]="08_results_1A_flexSC_flexLig"
OUTDIR[9]="09_results_2A_flexSC_flexLig"

RIGLIG[1]="1"
RIGLIG[2]="1"
RIGLIG[3]="1"
RIGLIG[4]="1"
RIGLIG[5]="1"
RIGLIG[6]="1"
RIGLIG[7]="0"
RIGLIG[8]="0"
RIGLIG[9]="0"

LIGRST[1]=false
LIGRST[2]=false
LIGRST[3]=false
LIGRST[4]=false
LIGRST[5]=false
LIGRST[6]=false
LIGRST[7]=true
LIGRST[8]=true
LIGRST[9]=true

FLEXSC[1]=false
FLEXSC[2]=false
FLEXSC[3]=false
FLEXSC[4]=true
FLEXSC[5]=true
FLEXSC[6]=true
FLEXSC[7]=true
FLEXSC[8]=true
FLEXSC[9]=true

STRUCTURES[1]="100"
STRUCTURES[2]="50"
STRUCTURES[3]="50"
STRUCTURES[4]="100"
STRUCTURES[5]="50"
STRUCTURES[6]="50"
STRUCTURES[7]="100"
STRUCTURES[8]="50"
STRUCTURES[9]="50"

RMSD[1]="0.5"
RMSD[2]="1"
RMSD[3]="2"
RMSD[4]="0.5"
RMSD[5]="1"
RMSD[6]="2"
RMSD[7]="0.5"
RMSD[8]="1"
RMSD[9]="2"

for i in 1 2 3 5 6 7 8 9; 
do
	cp plantsconfig_template plantsconfig
	echo "ligand_file "$LIGFILE >> plantsconfig
	echo "output_dir "${OUTDIR[i]} >> plantsconfig
	echo "rigid_ligand "${RIGLIG[i]} >> plantsconfig
	echo "cluster_structures "${STRUCTURES[i]} >> plantsconfig
	echo "cluster_rmsd "${RMSD[i]} >> plantsconfig 

	if ${FLEXSC[i]};
	then
	    echo "flexible_protein_side_chain_number 50" >> plantsconfig
	    echo "flexible_protein_side_chain_number 51" >> plantsconfig
	    echo "flexible_protein_side_chain_number 52" >> plantsconfig
	    echo "flexible_protein_side_chain_number 58" >> plantsconfig
	    echo "flexible_protein_side_chain_number 71" >> plantsconfig
	    echo "flexible_protein_side_chain_number 73" >> plantsconfig
	    echo "flexible_protein_side_chain_number 105" >> plantsconfig
	    echo "flexible_protein_side_chain_number 121" >> plantsconfig
	    echo "flexible_protein_side_chain_number 122" >> plantsconfig
	    echo "flexible_protein_side_chain_number 123" >> plantsconfig
	    echo "flexible_protein_side_chain_number 124" >> plantsconfig
	    echo "flexible_protein_side_chain_number 125" >> plantsconfig
	    echo "flexible_protein_side_chain_number 128" >> plantsconfig
	    echo "flexible_protein_side_chain_number 174" >> plantsconfig
	    echo "flexible_protein_side_chain_number 184" >> plantsconfig
	    echo "flexible_protein_side_chain_number 185" >> plantsconfig
	    echo "flexible_protein_side_chain_number 328" >> plantsconfig
	    echo "flexible_protein_side_chain_number 331" >> plantsconfig
	fi

	PLANTS1.2_64bit --mode screen plantsconfig

    cd ${OUTDIR[i]}
	mkdir pdbs_raw
	cp ../convertMol2ToPdb.r .
	R < convertMol2ToPdb.r --no-save "--args ${pdb} ${ligand} ${STRUCTURES[i]}"
    cd ..
done

