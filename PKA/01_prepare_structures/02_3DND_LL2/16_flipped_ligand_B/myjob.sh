#!/bin/sh
#PBS -N sim_short
#PBS -l select=1:ncpus=4

CPUS=4

export AMBERHOME=/struct/carlomag/skjaerve/software/amber11
export MPI_HOME=/usr
export LD_LIBRARY_PATH=/struct/carlomag/skjaerve/software/lib:$LD_LIBRARY_PATH

cd $workdir

lamboot
$AMBERHOME/bin/tleap -f build
$MPI_HOME/bin/mpirun -np $CPUS $AMBERHOME/bin/sander.MPI -O -i min.in -o min.out -p complex.prmtop -c complex.inpcrd -r min.rst -ref complex.inpcrd
$MPI_HOME/bin/mpirun -np $CPUS $AMBERHOME/bin/sander.MPI -O -i md.in -o md.out -p complex.prmtop -c min.rst -r md.rst -x md.nc -ref min.rst
$AMBERHOME/bin/ambpdb -p complex.prmtop < min.rst > min.amb.pdb
$AMBERHOME/bin/ptrajcomplex.prmtop tmp.ptraj
lamclean
lamhalt


