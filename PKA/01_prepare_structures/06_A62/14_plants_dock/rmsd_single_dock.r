source("/struct/carlomag/skjaerve/r-scripts/dock_funs.R")
library(bio3d)

## Correct orientations of the ligands (correct atom numbers!)
ligandfile <- "../ligand.amber.pdb"
ligand <- read.pdb(ligandfile)

## Plants output file (docked ligands)
dockresult <- "docked_ligands.mol2"

## Read the Mol2 file
binding.modes <- read.mol2(dockresult)

## Calcualte RMSD values
modes.rmsd <- rmsd(a=ligand$xyz, b=binding.modes$xyz, fit=FALSE)

## Print RMSD values
print(modes.rmsd)

num.1A <- length(which(modes.rmsd<1))
num.2A <- length(which(modes.rmsd<2))

## Draw histogram
pdf('rmsd_docking_modes.pdf')
h <- hist(modes.rmsd, xlab="RMSD (A)" )
at <- h$mids[1]-0.5
mtext( paste("Min. RMSD:", min(modes.rmsd), sep=" "), at=at, side=3, las=1, adj = 0, cex=0.8 )
mtext( paste("Num 2A:", num.2A, sep=" "), at=at, side=3, line=-1, las=1, adj = 0, cex=0.8 )
mtext( paste("Num 1A:", num.1A, sep=" "), at=at, side=3, line=-2, las=1, adj = 0, cex=0.8 )
dev.off()

