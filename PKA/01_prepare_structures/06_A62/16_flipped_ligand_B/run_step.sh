#!/bin/bash

ligand_resi="352"
protein_resi="1-351"
ref_mask=":50-52,57,58,60,70,71,73,92,105-106,121-128,171,172,174,182,184-186,328,331@N,C,CA"


## Copy files
cp ../11_build_ref/xray_remodeled_fit.pdb receptor.pdb
cp ../11_build_ref/prepi .
cp ../11_build_ref/frcmod .

cat <<eof >min.in
short minimisation
 &cntrl
 imin=1, maxcyc=1000, ncyc=700,
 cut=15., rgbmax=12., ntb=0,
 igb=2, saltcon=0.2,
 ntr=1, restraintmask=':${ligand_resi}',
 restraint_wt=1.0,
 ntpr=100
 /
eof

cat <<eof >md.in
short MD
 &cntrl
 imin=0,irest=0,ntx=1,
 nstlim=5000,dt=0.001,
 ntc=2,ntf=2,
 ntpr=50, ntwx=100,
 ntt=3, gamma_ln=2.0,
 tempi = 0, temp0=300.0,
 igb=5, saltcon=0.1,
 cut=12,rgbmax=10,
 ntb = 0,
 ntr=1, restraintmask=':${protein_resi}@N,CA,C & :${ligand_resi}',
 restraint_wt=1.0,
 ioutfm=1,
 /
eof

cat <<eof >tmp.ptraj
trajin md.nc
reference min.rst
rms reference mass ${ref_mask} 
trajout ../17_pdbs_MDs/frames_md_flipped_B pdb
eof


mkdir pdbs
qsub -v workdir=`pwd` myjob.sh
