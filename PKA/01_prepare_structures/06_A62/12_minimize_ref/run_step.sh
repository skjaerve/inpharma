#!/bin/bash

CPUS=2
ligand="A62"
ligand_resi="352"

ln -s ../11_build_ref/complex.prmtop
ln -s ../11_build_ref/complex.inpcrd

cat <<eof >min.in
short minimisation
 &cntrl
  imin=1, maxcyc=1000, ncyc=700,
  cut=15., rgbmax=12., ntb=0,
  igb=2, saltcon=0.2,
  ntr=1, restraintmask=':${ligand_resi}',
  restraint_wt=0.5,
  ntpr=100
/
eof

#$MPI_HOME/bin/mpirun -np $CPUS $AMBERHOME/bin/sander.MPI -O -i min.in -o min.out -p complex.prmtop -c complex.inpcrd -r min.rst -ref complex.inpcrd

ambpdb -p complex.prmtop < min.rst > min.amb.pdb
grep ${ligand} min.amb.pdb > ${ligand}.amb.pdb

cat <<eof >ptrajin
  trajin min.rst
  strip :${ligand_resi}
  trajout receptor.min.amb.rst restart nobox
eof

ptraj complex.prmtop ptrajin
mv receptor.min.amb.rst.1 receptor.min.amb.rst

ambpdb -p ../11_build_ref/receptor.prmtop < receptor.min.amb.rst > receptor.min.amb.pdb

# Convert to SYBYL mol2-files
top2mol2 -p ../11_build_ref/receptor.prmtop -c receptor.min.amb.rst -o receptor.sybyl.mol2

# Convert to PLANTS mol2-files
SPORES_64bit --mode settypes receptor.sybyl.mol2 receptor.plants.mol2

