#!/bin/bash

CPUS=4
ligand_resi="352"
protein_resi="1-351"
ref_mask=":50-52,57,58,60,70,71,73,92,105-106,121-128,171,172,174,182,184-186,328,331@N,C,CA"

cat <<eof >md.in
short MD
 &cntrl
 imin=0,irest=0,ntx=1,
 nstlim=5000,dt=0.001,
 ntc=2,ntf=2,
 ntpr=50, ntwx=100,
 ntt=3, gamma_ln=2.0,
 tempi = 0, temp0=300.0,
 igb=5, saltcon=0.1,
 cut=12,rgbmax=10,
 ntb = 0,
 ntr=1, restraintmask=':${protein_resi}@N,CA,C & :${ligand_resi}',
 restraint_wt=1.0,
 ioutfm=1,
 /
eof

$MPI_HOME/bin/mpirun -np $CPUS $AMBERHOME/bin/sander.MPI -O -i md.in -o md.out -p complex.prmtop -c min.rst -r md.rst -x md.nc -ref min.rst

mkdir pdbs
cd pdbs

cat <<eof >tmp.ptraj
trajin ../md.nc
reference ../min.rst
rms reference mass ${ref_mask} 
trajout frames_md_flipped_A pdb
eof

ptraj ../complex.prmtop tmp.ptraj

cd ..

