#!/bin/bash

export AMBERHOME=/struct/carlomag/skjaerve/software/amber11

ligand_name="LL1"
#ligand_resi="352"

base=`pwd`
inputfiles=${base}"/../00_input_files"

pdbdir="pdbs_raw"
pdbdir_min="pdbs_min"

if [ ! -d $pdbdir ] 
    then
    
    echo "Could not find directory "$pdbdir
    exit 1
fi

mkdir $pdbdir_min
cd $pdbdir

pdbfiles=`ls *.pdb`
echo $pdbfiles

i=0
for file in ${pdbfiles}
  do

  cd ${base}/${pdbdir}
  let i=${i}+1
  n=$(printf %03d $i)
  tmpdir=_tmp_min_${n}

  mkdir $tmpdir
  cd $tmpdir
  workdir=`pwd`
  echo ${workdir}

  ## Copy required files to the dir
  cp ../$file complex.pdb
  cp ${inputfiles}/* .

  grep ${ligand_name} complex.pdb > ligand.pdb
  grep -v ${ligand_name} complex.pdb > receptor.pdb
  
  outfile=${base}/${pdbdir_min}/${file}
  
  ## Run Amber
  qsub -v workdir=${workdir},outfile=${outfile} myjob.sh

  
done
