
# downloaded Xray PDB file
load xray.pdb

# reference PDB for structural alignment
load reference.amber.pdb, reference

# xray structure remodeled
load xray_model.pdb

align xray_model and resi 50-52+58+71+73+105+121-125+127+174+184+184, reference and resi 50-52+58+71+73+105+121-125+127+174+184+184
align xray, xray_model

save xray_remodeled_fit.pdb, xray_model

select ligand, resname LIGANDID
save ligand.pdb, ligand
