from modeller import *
from modeller.automodel import *


log.verbose()
env = environ()

# directories for input atom files
env.io.atom_files_directory = ['.']

a = automodel(env, alnfile='alignment.ali',
              knowns='XRAY', sequence='target',
              assess_methods=(assess.DOPE, assess.GA341))

a.md_level = None 
a.rand_method = None
a.max_var_iteration = 1

a.starting_model = 1
a.ending_model = 1
a.make()

