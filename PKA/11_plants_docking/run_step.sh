#!/bin/bash

ln -s ../01_prepare_structures/01_3DNE_LL1/14_plants_dock 01_3DNE_LL1
ln -s ../01_prepare_structures/02_3DND_LL2/14_plants_dock 02_3DND_LL2 
ln -s ../01_prepare_structures/03_1Q8W_M77/14_plants_dock 03_1Q8W_M77
ln -s ../01_prepare_structures/04_2VNY_M02/14_plants_dock 04_2VNY_M02
ln -s ../01_prepare_structures/05_A14/14_plants_dock 05_A14
ln -s ../01_prepare_structures/06_A62/14_plants_dock 06_A62
