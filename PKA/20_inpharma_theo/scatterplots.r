
library(plotrix)
source("/struct/carlomag/skjaerve/r-scripts/inpharma.R")


result.suffix <- "/02_allRow/noes.dat"

inph.dirs <- c("02_LL1-LL2", "03_LL1-M77", "04_LL1-M02","05_LL1-A14", "06_LL1-A62",
               "07_LL2-M77", "08_LL2-M02","09_LL2-A14", "10_LL2-A62",
               "11_M77-M02", "12_M77-A14", "13_M77-A62",
               "14_M02-A14", "15_M02-A62", "16_A14-A62")


all.data <- NULL
for ( i in 1:length(inph.dirs) ) {

  datafile <- paste(inph.dirs[i], result.suffix, sep="")
  data <- read.table(datafile, header=T)
  all.data[[i]] <- data
}


pdf("scatterplots.pdf", w=20, h=12)
par(mfrow=c(3,5), mar=c(3,2,3,2))

for ( i in 1:length(inph.dirs) ) {
  data <- all.data[[i]]
  mtext <- inph.dirs[i]
  
  plot.filter.hits(data, num.best=100,
                   ylim=c(0.8, 1), lnp.cutoff=5,
                   slope.cutoff=c(0,2), q.cutoff=2,
                   mtext=mtext)
}

dev.off()





